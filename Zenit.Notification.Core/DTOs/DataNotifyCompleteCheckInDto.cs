﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Core.DTOs
{
    public class DataNotifyCompleteCheckInDto
    {
        public int? UserId { get; set; }
        public string? User { get; set; }
        public string? Email { get; set; }



    }
}
