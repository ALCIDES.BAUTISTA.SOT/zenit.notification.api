﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenit.Notification.Core.Entities;
using Zenit.Notification.Core.Helper;
using Zenit.Notification.Core.Interfaces;

namespace Zenit.Notification.Core.Services
{
    public class NotificationServices : INotificationEmailServices

    {
        private readonly IConfiguration _configuration;
        private readonly INotificationEmailRepository _notificationEmailesRepository;
        private readonly LoginMail _loginMail;

        public NotificationServices(IConfiguration config, INotificationEmailRepository notificationEmailesRepository)
        {
            _configuration = config;
            _notificationEmailesRepository = notificationEmailesRepository;

            _loginMail = new LoginMail(_configuration);
        }

       

        public async Task<bool> GetDataNotifyCompleteCheckIn(int MailId, int userId, int systemId)
        {
            NotificationEmail email = await _notificationEmailesRepository.GetTemplateMail(MailId);

            var dataNotifyCompleteFile = await _notificationEmailesRepository.GetDataNotifyCompleteCheckIn(MailId, userId);
            var parameters = await _notificationEmailesRepository.ParametersMail(systemId);

            string cc = email.CC ?? string.Empty;
            string sBody = string.Format(email.Body, dataNotifyCompleteFile.User, dataNotifyCompleteFile.UserId);


            bool successfulNotification = await _loginMail.EnvioEmail(email.From, dataNotifyCompleteFile.Email, cc, string.Empty, email.Subject, sBody, new List<string>(), parameters);

            return successfulNotification;

        }

        public async Task<bool> GetDataProvidersNotifyCompleteCheckIn(int MailId, int userId, int systemId)
        {
            NotificationEmail email = await _notificationEmailesRepository.GetTemplateProviderMail(MailId);

            var dataNotifyCompleteFile = await _notificationEmailesRepository.GetDataProvidersNotifyCompleteCheckIn(MailId, userId);
            var parameters = await _notificationEmailesRepository.ParametersMail(systemId);

            string cc = email.CC ?? string.Empty;
            string sBody = string.Format(email.Body, dataNotifyCompleteFile.User, dataNotifyCompleteFile.UserId);


            bool successfulNotification = await _loginMail.EnvioEmail(email.From, dataNotifyCompleteFile.Email, cc, string.Empty, email.Subject, sBody, new List<string>(), parameters);

            return successfulNotification;
        }
    }
}
