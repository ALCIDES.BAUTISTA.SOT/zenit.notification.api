﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenit.Notification.Core.Entities;

namespace Zenit.Notification.Core.Interfaces
{
   public interface INotificationEmailRepository
    {
        Task<NotificationEmail> GetTemplateMail(int mailId);
        
        Task<DataNotifyCompleteCheckIn> GetDataNotifyCompleteCheckIn(int MailId, int userId);

        Task<List<Parameter>> ParametersMail(int systemId);

        Task<DataNotifyCompleteCheckIn> GetDataProvidersNotifyCompleteCheckIn(int MailId,int userId);

        Task<NotificationEmail> GetTemplateProviderMail(int mailId);

           
    }
}
