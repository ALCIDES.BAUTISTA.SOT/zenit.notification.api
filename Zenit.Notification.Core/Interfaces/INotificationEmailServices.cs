﻿
using Zenit.Notification.Core.Entities;

namespace Zenit.Notification.Core.Interfaces
{
    public interface INotificationEmailServices
    {
        Task<bool> GetDataNotifyCompleteCheckIn(int MailId,int UserId, int SystemId);

        Task<bool> GetDataProvidersNotifyCompleteCheckIn(int MailId, int UserId, int SystemId);
       
    }
}
