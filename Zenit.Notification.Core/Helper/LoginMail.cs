﻿using j.GAM.Utility;
using Microsoft.Extensions.Configuration;
using Zenit.Notification.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Zenit.Notification.Core.Helper
{
    public class LoginMail
    {
        private readonly IConfiguration _configuration;

        public LoginMail(IConfiguration config)
        {
            _configuration = config;
        }

        public Task<bool> EnvioEmail(string From, string ToAdd, string CC, string CCO, string Subject, string Body, List<string> Attachments, List<Parameter> ListParameters)
        {
            string valueParameter = string.Empty, section = string.Empty, msgError = string.Empty;
            int puertosmtp, servidor, correo, contrasena, isSSL, servidorCorreo;
            bool blnOK = false, enableSSL = false;

            //Cambio de destinatarios para pruebas de desarrollo
            if (_configuration.GetSection("ApplicationSettings").GetSection("DevelopmentTestingEnvironment").Value.ToString().ToLower() == "true")
            {
                ToAdd = _configuration.GetSection("ApplicationSettings").GetSection("TestMail").Value.ToString();
            }

           // section = string.Format("SystemId.{0}", systemId);
            puertosmtp = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamPuertoSMTP").Value);
            servidor = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamServidorSMTP").Value);
            correo = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamCredencialUsuarioSMTP").Value);
            contrasena = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamCredencialContrasenaSMTP").Value);
            isSSL = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamIsSSL").Value);
            servidorCorreo = int.Parse(_configuration.GetSection("ApplicationSettings").GetSection("ParamServidorCorreo").Value);

            //Servidor de correo
            string dominio = String.Empty;

            //servidor
            string smtpAddress = (from parameters in ListParameters
                                  where parameters.ParameterId == servidor
                                  select parameters.Value).First();

            //correo
            valueParameter = (from parameters in ListParameters
                              where parameters.ParameterId == correo
                              select parameters.Value).First();

            string emailFrom = "notificaciones@sygno.com.mx";
                //CCrypto.Decrypt(valueParameter, DConfig.C_PassPhrase, DConfig.C_SaltValue, DConfig.C_HashAlgorithm, DConfig.C_PasswordIterations, DConfig.C_InitVector, DConfig.C_KeySize);
           
            //contrasena
            valueParameter = (from parameters in ListParameters
                              where parameters.ParameterId == contrasena
                              select parameters.Value).First();

            string password = CCrypto.Decrypt(valueParameter, DConfig.C_PassPhrase, DConfig.C_SaltValue, DConfig.C_HashAlgorithm, DConfig.C_PasswordIterations, DConfig.C_InitVector, DConfig.C_KeySize);

            //puertosmtp
            string portNumber = (from parameters in ListParameters
                                 where parameters.ParameterId == puertosmtp
                                 select parameters.Value).First();

            //isSSL 
            string isPortSSL = (from parameters in ListParameters
                                where parameters.ParameterId == isSSL
                                select parameters.Value).First();

            enableSSL = isPortSSL != "0";

            blnOK = CMail.EnviarCorreo(string.IsNullOrEmpty(From) ? emailFrom : From, ToAdd.Split(';'), Subject,
                                                          Body.ToString(), string.IsNullOrEmpty(CC) ? new string[] { } : CC.Split(';'), string.IsNullOrEmpty(CCO) ? new string[] { } : CCO.Split(';'), MailPriority.High, true,
                                                          Attachments.ToArray(), smtpAddress, portNumber.ToString(), emailFrom, password, dominio, enableSSL, ref msgError);

            return Task.FromResult(blnOK);
        }
    }
}
