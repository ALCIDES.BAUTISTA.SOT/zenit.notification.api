﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Core.Helper
{
    public class DConfig
    {
        //Valores constantes para Encriptacion y desencriptacion
        public const string C_PassPhrase = "Pas5pr@se";
        public const string C_SaltValue = "s@1tValue";
        public const string C_HashAlgorithm = "SHA1";
        public const int C_PasswordIterations = 2;
        public const string C_InitVector = "@1B2c3D4e5F6g7H8";
        public const int C_KeySize = 256;
    }
}
