﻿

namespace Zenit.Notification.Core.Entities
{
    public class Parameter
    {
        public int? ParameterId { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public int? SystemId { get; set; }


        public static implicit operator Task<object>(Parameter v)
        {
            throw new NotImplementedException();
        }
    }
}
