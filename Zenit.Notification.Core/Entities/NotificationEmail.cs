﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Core.Entities
{
    public class NotificationEmail
    {
        public int? MailId { get; set; }


     // public byte? SystemId { get; set; }
        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string CC { get; set; }
       // public string CCO { get; set; }
    }
}
