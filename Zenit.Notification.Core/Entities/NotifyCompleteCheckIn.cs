﻿

namespace Zenit.Notification.Core.Entities
{
    public class DataNotifyCompleteCheckIn
    {
        public int? UserId { get; set; }
        public string? User { get; set; }
        public string? Email { get; set; }

        public string? Link { get; set; }
    }
}
