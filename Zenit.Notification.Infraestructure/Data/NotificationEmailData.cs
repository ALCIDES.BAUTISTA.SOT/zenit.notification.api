﻿    using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenit.Notification.Core.Entities;
using Zenit.Notification.Infraestructure.Helper;

namespace Zenit.Notification.Infraestructure.Data
{
    public class NotificationEmailData
    {

        private readonly DbContextData _dbContextData;

        public NotificationEmailData(IConfiguration config)
        {
            _dbContextData = new DbContextData(config);
        }

        public async Task<NotificationEmail> GetTemplateProviderMail(int mailId)
        {
            var notificationEmail = new NotificationEmail();
            var parameters = new ArrayList
            {
                new SqlParameter { ParameterName = "@MailId", SqlDbType = SqlDbType.Int, Value = mailId }
                };

            DataSet ds = await _dbContextData.FillAsync(Constants.DataBase.ZENIT7_DEV, "notificacion.spSelMail", parameters);

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    notificationEmail.MailId = row.ToIntNullable("MailId");
                    notificationEmail.Subject = row.ToStringNullable("Asunto");
                    notificationEmail.Body = row.ToStringNullable("Cuerpo");
                    notificationEmail.From = row.ToStringNullable("De");
                    notificationEmail.To = row.ToStringNullable("Para");
                    notificationEmail.CC = row.ToStringNullable("CC");
                    // notificationEmail.CCO = row.ToStringNullable("CCO");
                }
            }

            return notificationEmail;
        }

        public async Task<NotificationEmail> GetTemplateMail(int mailId)
        {
            var notificationEmail = new NotificationEmail();
            var parameters = new ArrayList
            {
                new SqlParameter { ParameterName = "@MailId", SqlDbType = SqlDbType.Int, Value = mailId }
                };

            DataSet ds = await _dbContextData.FillAsync(Constants.DataBase.ZENIT7_DEV, "notificacion.spSelMail", parameters);

           if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    notificationEmail.MailId = row.ToIntNullable("MailId");
                    notificationEmail.Subject = row.ToStringNullable("Asunto");
                    notificationEmail.Body = row.ToStringNullable("Cuerpo");
                    notificationEmail.From = row.ToStringNullable("De");
                    notificationEmail.To = row.ToStringNullable("Para");
                    notificationEmail.CC = row.ToStringNullable("CC");
                   // notificationEmail.CCO = row.ToStringNullable("CCO");
                }
            }

            return notificationEmail;
        }

        public async Task<List<Parameter>> ParametersMail(int systemId)
        {
            var listParameters = new List<Parameter>();

            var parameters = new ArrayList
            {
                new SqlParameter { ParameterName = "@SistemaId", SqlDbType = SqlDbType.Int, Value = systemId }
            };

            DataSet ds = await _dbContextData.FillAsync(Constants.DataBase.ZENIT7_DEV, "notificacion.spSelParametrosMailBySistemaId", parameters);

            if (ds.Tables.Count <= 0) return listParameters;
            listParameters.AddRange(from DataRow row in ds.Tables[0].Rows
                                    select new Parameter
                                    {
                                        ParameterId = row.ToIntNullable("ParametroId"),
                                        Name = row.ToStringNullable("Nombre"),
                                        Value = row.ToStringNullable("Valor"),
                                        SystemId = row.ToIntNullable("SistemaId")
                                    });

            return listParameters;
        }

        public async Task<DataNotifyCompleteCheckIn> GetDataProvidersNotifyCompleteCheckIn(int UserId)
        {
            var notificationEmail = new DataNotifyCompleteCheckIn();

            var parameters = new ArrayList
            {
                new SqlParameter { ParameterName = "@UsuarioId", SqlDbType = SqlDbType.Int, Value = UserId },
                //new SqlParameter{ ParameterName = "@Nombre", SqlDbType=SqlDbType.VarChar, Size=50, Value= data.User  },
                //new SqlParameter{ ParameterName = "@Correo", SqlDbType=SqlDbType.VarChar, Size=100, Value= data.Email  }

                 };

            DataSet ds = await _dbContextData.FillAsync(Constants.DataBase.ZENIT7_DEV, "notificacion.spSelProveedores", parameters);

            if (ds.Tables.Count <= 0) ;
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                notificationEmail.UserId = row.ToIntNullable("UsuarioId");
                notificationEmail.User = row.ToStringNullable("Nombre");
                notificationEmail.Email = row.ToStringNullable("Correo");
                //  Link = row.ToStringNullable("Liga")
            };


            return notificationEmail;
        }



        public async Task<DataNotifyCompleteCheckIn> GetDataNotifyCompleteCheckIn(int UserId)
        {
            var notificationEmail = new DataNotifyCompleteCheckIn();
                          
            var parameters = new ArrayList
            {
                new SqlParameter { ParameterName = "@UsuarioId", SqlDbType = SqlDbType.Int, Value = UserId },
                //new SqlParameter{ ParameterName = "@Nombre", SqlDbType=SqlDbType.VarChar, Size=50, Value= data.User  },
                //new SqlParameter{ ParameterName = "@Correo", SqlDbType=SqlDbType.VarChar, Size=100, Value= data.Email  }

                 };

            DataSet ds = await _dbContextData.FillAsync(Constants.DataBase.ZENIT7_DEV, "notificacion.spSelEmpresa", parameters);

            if (ds.Tables.Count <= 0);
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                notificationEmail.UserId = row.ToIntNullable("UsuarioId");
                notificationEmail.User = row.ToStringNullable("Nombre");
                notificationEmail.Email = row.ToStringNullable("Correo");
                  //  Link = row.ToStringNullable("Liga")
                };  
            

            return notificationEmail;
        }

    }

}
