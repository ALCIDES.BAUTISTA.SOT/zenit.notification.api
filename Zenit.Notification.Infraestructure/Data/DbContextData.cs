﻿using j.GAM.Utility;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Zenit.Notification.Core.Helper;

namespace Zenit.Notification.Infraestructure.Data
{
    public class DbContextData
    {
        public const string DBZENIT7_DEV = "ZENIT7_DEV";

        private readonly IConfiguration _configuration;
        private readonly SqlConnection sqlConnection;
        private readonly SqlConnection sqlConnection2;
        private readonly SqlConnection sqlConnection3;
        private readonly SqlConnection sqlConnection4;
        private readonly SqlConnection sqlConnectionTemplateMail;
        private readonly SqlConnection sqlConnectionParametersMail;

        private SqlTransaction transaction;
        private SqlTransaction transaction2;
        private SqlTransaction transaction3;
        private SqlTransaction transaction4;
        private SqlTransaction transactionTemplateMail;
        private SqlTransaction transactionParametersMail;

        public DbContextData(IConfiguration config)
        {
            _configuration = config;
            sqlConnection = new SqlConnection();
            sqlConnection2 = new SqlConnection();
            sqlConnection3 = new SqlConnection();
            sqlConnection4 = new SqlConnection();
            sqlConnectionTemplateMail = new SqlConnection();
            sqlConnectionParametersMail = new SqlConnection();
        }

        private string GetConnectionStrings(string bd)
        {
            return bd switch
            {
                DBZENIT7_DEV => CCrypto.Decrypt(_configuration.GetConnectionString("CnnString"), DConfig.C_PassPhrase, DConfig.C_SaltValue, DConfig.C_HashAlgorithm, DConfig.C_PasswordIterations, DConfig.C_InitVector, DConfig.C_KeySize),
                _ => string.Empty,
            };
        }

        private void OpenConnection(string database)
        {
            if (sqlConnection.State == ConnectionState.Open) return;
            sqlConnection.ConnectionString = GetConnectionStrings(database);
            sqlConnection.Open();
        }

        private void CloseConnection()
        {
            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }
        }

        private void OpenConnection2(string database)
        {
            if (sqlConnection2.State == ConnectionState.Open) return;
            sqlConnection2.ConnectionString = GetConnectionStrings(database);
            sqlConnection2.Open();
        }

        private void CloseConnection2()
        {
            if (sqlConnection2.State == ConnectionState.Open)
            {
                sqlConnection2.Close();
            }
        }

        private void OpenConnection3(string database)
        {
            if (sqlConnection3.State == ConnectionState.Open) return;
            sqlConnection3.ConnectionString = GetConnectionStrings(database);
            sqlConnection3.Open();
        }

        private void CloseConnection3()
        {
            if (sqlConnection3.State == ConnectionState.Open)
            {
                sqlConnection3.Close();
            }
        }

        private void OpenConnection4(string database)
        {
            if (sqlConnection4.State == ConnectionState.Open) return;
            sqlConnection4.ConnectionString = GetConnectionStrings(database);
            sqlConnection4.Open();
        }

        private void CloseConnection4()
        {
            if (sqlConnection4.State == ConnectionState.Open)
            {
                sqlConnection4.Close();
            }
        }

        private void OpenConnectionTemplateMail(string database)
        {
            if (sqlConnectionTemplateMail.State == ConnectionState.Open) return;
            sqlConnectionTemplateMail.ConnectionString = GetConnectionStrings(database);
            sqlConnectionTemplateMail.Open();
        }

        private void CloseConnectionTemplateMail()
        {
            if (sqlConnectionTemplateMail.State == ConnectionState.Open)
            {
                sqlConnectionTemplateMail.Close();
            }
        }


        private void OpenConnectionParametersMail(string database)
        {
            if (sqlConnectionParametersMail.State == ConnectionState.Open) return;
            sqlConnectionParametersMail.ConnectionString = GetConnectionStrings(database);
            sqlConnectionParametersMail.Open();
        }

        private void CloseConnectionParametersMail()
        {
            if (sqlConnectionParametersMail.State == ConnectionState.Open)
            {
                sqlConnectionParametersMail.Close();
            }
        }

        public void ExecuteNonQuery(string database, string procedure, ArrayList parameters)
        {
            try
            {
                OpenConnection(database);

                using SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure };
                foreach (SqlParameter param in parameters)
                {
                    mComando.Parameters.Add(param);
                }

                mComando.ExecuteNonQuery();
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public DataSet Fill(string database, string procedure, ArrayList parameters)
        {
            DataSet mDataSet = new DataSet();

            try
            {
                OpenConnection(database);

                using (SqlCommand mComando = new SqlCommand(procedure, sqlConnection))
                {
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter param in parameters)
                    {
                        mComando.Parameters.Add(param);
                    }

                    mDataAdapter.Fill(mDataSet);
                }

                return mDataSet;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public SqlDataReader ExecuteReader(string database, string procedure, ArrayList parameters)
        {
            SqlDataReader dataReader;
            try
            {
                OpenConnection(database);

                using (SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure })
                {
                    foreach (SqlParameter param in parameters)
                    {
                        mComando.Parameters.Add(param);
                    }

                    dataReader = mComando.ExecuteReader();
                }

                return dataReader;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public int ExecuteScalar(string database, string procedure, ArrayList parameters)
        {
            try
            {
                OpenConnection(database);

                using SqlCommand mComando = new SqlCommand(procedure, sqlConnection) { CommandType = CommandType.StoredProcedure };
                foreach (SqlParameter param in parameters)
                {
                    mComando.Parameters.Add(param);
                }

                return Convert.ToInt32(mComando.ExecuteScalar()); ;
            }
            catch
            {
                CloseConnection();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        // Exclusive FillAsync
        public Task<DataSet> FillAsyncTemplateMail(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnectionTemplateMail(database);
                    transactionTemplateMail = sqlConnection.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transactionTemplateMail;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transactionTemplateMail.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transactionTemplateMail.Rollback();
                CloseConnectionTemplateMail();
                throw;
            }
            //finally
            //{
            //    CloseConnectionTemplateMail();
            //}
        }

        public Task<DataSet> FillAsyncParametersMail(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnectionParametersMail(database);
                    transactionParametersMail = sqlConnection.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transactionParametersMail;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transactionParametersMail.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transactionParametersMail.Rollback();
                CloseConnectionParametersMail();
                throw;
            }
            finally
            {
                CloseConnectionParametersMail();
            }
        }



        // Generic 
        public Task<DataSet> FillAsync(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnection(database);
                    transaction = sqlConnection.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transaction;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transaction.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transaction.Rollback();
                CloseConnection();
                throw;
            }
            finally
            {
                //CloseConnection();
            }
        }

        public Task<DataSet> FillAsync2(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnection2(database);
                    transaction2 = sqlConnection2.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection2);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transaction2;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transaction2.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transaction2.Rollback();
                CloseConnection2();
                throw;
            }
            finally
            {
                CloseConnection2();
            }
        }

        public Task<DataSet> FillAsync3(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnection3(database);
                    transaction3 = sqlConnection3.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection3);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transaction3;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transaction3.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transaction3.Rollback();
                CloseConnection3();
                throw;
            }
            finally
            {
                CloseConnection3();
            }
        }

        public Task<DataSet> FillAsync4(string database, string procedure, ArrayList parameters)
        {
            try
            {
                return Task.Run(() =>
                {
                    OpenConnection4(database);
                    transaction4 = sqlConnection4.BeginTransaction("Transaction.Notification.API");

                    using SqlCommand mComando = new SqlCommand(procedure, sqlConnection4);
                    using SqlDataAdapter mDataAdapter = new SqlDataAdapter(mComando);
                    mComando.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            mComando.Parameters.Add(param);
                        }
                    }

                    mComando.Transaction = transaction4;

                    DataSet mDataSet = new DataSet();
                    mDataAdapter.Fill(mDataSet);

                    transaction4.Commit();

                    return mDataSet;
                });
            }
            catch
            {
                transaction4.Rollback();
                CloseConnection4();
                throw;
            }
            finally
            {
                CloseConnection4();
            }
        }
    }
}
