﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Infraestructure.Helper
{
    public static class ConvertDBValue
    {
        public static int? ToIntNullable(this DataRow row, string columnName)
        {
            return CastAsIntNullable(row[columnName]);
        }

        private static int? CastAsIntNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            int? value = hasValue ? (int?)o : null;
            return value;
        }

        public static string ToStringNullable(this DataRow row, string columnName)
        {
            return CastAsStringNullable(row[columnName]);
        }

        private static string CastAsStringNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            string value = hasValue ? o.ToString() : null;
            return value;
        }

        public static DateTime? ToDateNullable(this DataRow row, string columnName)
        {
            return CastAsDateNullable(row[columnName]);
        }

        private static DateTime? CastAsDateNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            DateTime? value = hasValue ? (DateTime?)o : null;
            return value;
        }

        public static decimal? ToDecimalNullable(this DataRow row, string columnName)
        {
            return CastAsDecimalNullable(row[columnName]);
        }

        private static decimal? CastAsDecimalNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            decimal? value = hasValue ? (decimal?)o : null;
            return value;
        }

        public static short? ToInt16Nullable(this DataRow row, string columnName)
        {
            return CastAsInt16Nullable(row[columnName]);
        }

        private static short? CastAsInt16Nullable(object o)
        {
            bool hasValue = !(o is DBNull);
            short? value = hasValue ? (short?)o : null;
            return value;
        }

        public static byte? ToByteNullable(this DataRow row, string columnName)
        {
            return CastAsByteNullable(row[columnName]);
        }

        private static byte? CastAsByteNullable(object o)
        {
            bool hasValue = !(o is DBNull);
            byte? value = hasValue ? (byte?)o : null;
            return value;
        }
    }
}

