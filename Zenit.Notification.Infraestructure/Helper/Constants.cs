﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Infraestructure.Helper
{
    public class Constants
    {
        public static class DataBase
        {
            public const string ZENIT7_DEV = "ZENIT7_DEV";
        }
        public static class Message
        {
            public const string M100 = "Success";
            public const string M101 = "Review log records";
        }

        public static class Accion
        {
            public const string A101 = "Respuesta Solicitud";
            public const string A102 = "Nueva Solicitud";
            public const string A103 = "Insertar";
            public const string A104 = "Actualizar";
            public const string A105 = "Eliminar";
            public const string A106 = "Seleccionar";

            public const string A999 = "Error";
        }
        public static class MailTemplates
        {
            public const int MT1_ConfirmRegistration = 1;
            public const int MT2_ChangePassword = 2;

        }

        public static class Systems
        {
            public const int ZENIT7 = 1;

        }
    }
}
