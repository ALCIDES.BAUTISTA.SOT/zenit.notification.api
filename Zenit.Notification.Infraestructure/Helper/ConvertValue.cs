﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenit.Notification.Infraestructure.Helper
{
    public static class ConvertValue
    {
        //public static DataTable ConvertToDataTable<T>(IList<T> data)
        //{
        //    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

        //    DataTable table = new DataTable();

        //    foreach (PropertyDescriptor prop in properties)
        //    {
        //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //    }

        //    foreach (T item in data)
        //    {
        //        DataRow row = table.NewRow();
        //        foreach (PropertyDescriptor prop in properties) row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //        { 
        //            table.Rows.Add(row); 
        //        }
        //    }

        //    return table;
        //}

        //public static DataTable ToDataTable<T>(IList<T> list)
        //{
        //    PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
        //    DataTable table = new DataTable();
        //    for (int i = 0; i < props.Count; i++)
        //    {
        //        PropertyDescriptor prop = props[i];
        //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //    }
        //    object[] values = new object[props.Count];
        //    foreach (T item in list)
        //    {
        //        for (int i = 0; i < values.Length; i++)
        //            values[i] = props[i].GetValue(item) ?? DBNull.Value;
        //        table.Rows.Add(values);
        //    }
        //    return table;
        //}

        //public static DataTable ToDataTable<T>(List<T> iList)
        //{
        //    DataTable dataTable = new DataTable();
        //    PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(typeof(T));

        //    for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        //    {
        //        PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
        //        Type type = propertyDescriptor.PropertyType;

        //        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        //            type = Nullable.GetUnderlyingType(type);

        //        dataTable.Columns.Add(propertyDescriptor.Name, type);
        //    }

        //    object[] values = new object[propertyDescriptorCollection.Count];
        //    foreach (T iListItem in iList)
        //    {
        //        for (int i = 0; i < values.Length; i++)
        //        {
        //            values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
        //        }
        //        dataTable.Rows.Add(values);
        //    }
        //    return dataTable;
        //}

        //public static DataTable ListToDataTable<T>(IList<T> data)
        //{
        //    DataTable table = new DataTable();

        //    //special handling for value types and string
        //    if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
        //    {

        //        DataColumn dc = new DataColumn("Value");
        //        table.Columns.Add(dc);
        //        foreach (T item in data)
        //        {
        //            DataRow dr = table.NewRow();
        //            dr[0] = item;
        //            table.Rows.Add(dr);
        //        }
        //    }
        //    else
        //    {
        //        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        //        foreach (PropertyDescriptor prop in properties)
        //        {
        //            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //        }
        //        foreach (T item in data)
        //        {
        //            DataRow row = table.NewRow();
        //            foreach (PropertyDescriptor prop in properties)
        //            {
        //                try
        //                {
        //                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        //                }
        //                catch (Exception ex)
        //                {
        //                    row[prop.Name] = DBNull.Value;
        //                }
        //            }
        //            table.Rows.Add(row);
        //        }
        //    }
        //    return table;
        //}

        public static DataTable ListToDataTable<T>(IList<T> lst)
        {

            DataTable currentDT = CreateTable<T>("dt");

            Type entType = typeof(T);

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (T item in lst)
            {
                DataRow row = currentDT.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {

                    if (prop.PropertyType == typeof(decimal?) || prop.PropertyType == typeof(int?) || prop.PropertyType == typeof(long?))
                    {
                        if (prop.GetValue(item) == null)
                            row[prop.Name] = 0;
                        else
                            row[prop.Name] = prop.GetValue(item);
                    }
                    else
                        row[prop.Name] = prop.GetValue(item);

                }
                currentDT.Rows.Add(row);
            }

            return currentDT;
        }

        public static DataTable CreateTable<T>(string DTName)
        {
            Type entType = typeof(T);
            DataTable tbl = new DataTable(DTName);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.PropertyType == typeof(decimal?))
                    tbl.Columns.Add(prop.Name, typeof(decimal));
                else if (prop.PropertyType == typeof(int?))
                    tbl.Columns.Add(prop.Name, typeof(int));
                else if (prop.PropertyType == typeof(long?))
                    tbl.Columns.Add(prop.Name, typeof(long));
                else
                    tbl.Columns.Add(prop.Name, prop.PropertyType);
            }
            return tbl;
        }
    }
}
