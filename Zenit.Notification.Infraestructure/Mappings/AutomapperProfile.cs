﻿using AutoMapper;
using Zenit.Notification.Core.DTOs;
using Zenit.Notification.Core.Entities;

namespace Zenit.Notification.Infraestructure.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<NotificationEmail, GetNotificationEmailDto>();
            CreateMap<GetNotificationEmailDto, NotificationEmail>();

            CreateMap<DataNotifyCompleteCheckInDto, DataNotifyCompleteCheckIn>();
            CreateMap<DataNotifyCompleteCheckIn, DataNotifyCompleteCheckInDto>();
        }
    }
}
