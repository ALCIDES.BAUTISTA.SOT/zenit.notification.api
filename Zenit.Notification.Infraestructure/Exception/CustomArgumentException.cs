﻿using System.Runtime.Serialization;

namespace Zenit.Notification.Infraestructure.Exception
{
    [Serializable]
    public class CustomArgumentException : ArgumentException
    {
        public string Code { get; set; }

        public CustomArgumentException()
        {
        }

        public CustomArgumentException(string message) : base(message)
        {
        }

        public CustomArgumentException(string message, ArgumentException innerException) : base(message, innerException)
        {
        }

        protected CustomArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public CustomArgumentException(string _code, string message) : base(message)
        {
            Code = _code;
        }
    }
}

