﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zenit.Notification.Core.Entities;
using Zenit.Notification.Core.Interfaces;
using Zenit.Notification.Infraestructure.Data;

namespace Zenit.Notification.Infraestructure.Repositories
{
    public class NotificationEmailRepository: INotificationEmailRepository
    {
        private readonly IConfiguration _configuration;
        private readonly NotificationEmailData _NotificationEmailData;

        public NotificationEmailRepository(IConfiguration config)
        {
            _configuration = config;
            _NotificationEmailData = new NotificationEmailData(_configuration);
        }

        public async Task<DataNotifyCompleteCheckIn> GetDataNotifyCompleteCheckIn(int MailId, int userId)
        {
            return await _NotificationEmailData.GetDataNotifyCompleteCheckIn(userId);
        }

        public async Task<DataNotifyCompleteCheckIn> GetDataProvidersNotifyCompleteCheckIn(int MailId, int userId)
        {
            return await _NotificationEmailData.GetDataProvidersNotifyCompleteCheckIn(userId);
        }

        public async Task<NotificationEmail> GetTemplateMail(int mailId)
        {
            return await _NotificationEmailData.GetTemplateMail(mailId);
        }

        public async Task<NotificationEmail> GetTemplateProviderMail(int mailId)
        {
            return await _NotificationEmailData.GetTemplateProviderMail(mailId);
        }

        public async Task<List<Parameter>> ParametersMail(int systemId)
        {
            return await _NotificationEmailData.ParametersMail(systemId);
        }
    }
}
