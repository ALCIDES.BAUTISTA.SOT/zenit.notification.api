using Microsoft.OpenApi.Models;
using System.Reflection;
using Zenit.Notification.Core.Interfaces;
using Zenit.Notification.Core.Services;
using Zenit.Notification.Infraestructure.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// Add services to the container.
builder.Services.AddControllers();

//builder.Services.AddCors(options =>
//{
//    options.AddPolicy("Zenit",
//        builder =>
//        {
//            builder.WithOrigins("http://localhost:7211")
//            .AllowAnyHeader()
//            .AllowAnyMethod();
//        });
//});

builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("http://localhost:3000").AllowAnyMethod().AllowAnyHeader();
}));



//DI
builder.Services.AddTransient<INotificationEmailServices, NotificationServices>();
builder.Services.AddTransient<INotificationEmailRepository, NotificationEmailRepository>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Zenit.Notification,API",
        Description = "An ASP.NET Core Web API for managing Notification"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint(url: "./swagger/v1/swagger.json", name: "Zenit.Notification.API");
        options.InjectStylesheet("./swagger-ui/custom.css");
        options.RoutePrefix = string.Empty;
    });
//}

app.UseCors("corsapp");
app.UseHttpsRedirection();
app.UseAuthorization();

app.UseStaticFiles();

app.MapControllers();

app.Run();