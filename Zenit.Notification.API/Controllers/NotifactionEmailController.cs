﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Serilog;
using Zenit.Notification.Core.Interfaces;
using Zenit.Notification.Infraestructure.Helper;
using System.Security.Permissions;

namespace Zenit.Notification.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotifactionEmailController : ControllerBase
    {
        private IConfiguration Configuration { get; }
        private readonly INotificationEmailServices _notificationEmailService;
        private readonly IMapper _mapper;

        public NotifactionEmailController(IConfiguration configuration, INotificationEmailServices notificationEmailService, IMapper mapper)
        {
            Configuration = configuration;
            _notificationEmailService = notificationEmailService;
            _mapper = mapper;
        }
        /// <summary>
        /// Send return notification to billing
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("NotifyCompleteFile")]
        public async Task<IActionResult> GetDataNotifyCompleteCheckIn(int UserId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var successfulNotification = false;

                    successfulNotification = await _notificationEmailService.GetDataNotifyCompleteCheckIn(Constants.MailTemplates.MT1_ConfirmRegistration, UserId, Constants.Systems.ZENIT7);

                    return new Result.OKResult(successfulNotification);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string error = message.ErrorMessage != string.Empty
                        ? message.ErrorMessage
                        : message.Exception?.Message;

                    if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                            .ToLower() == "true")
                        Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                        .ToLower() == "true")
                    Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404",
                    ex.InnerException != null ? ex.InnerException.ToString() : ex.Message.ToString());
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        [HttpGet]
        [Route("NotifyChangePass")]
        public async Task<IActionResult> GetDataNotifyChangePass(int UserId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var successfulNotification = false;

                    successfulNotification = await _notificationEmailService.GetDataNotifyCompleteCheckIn(Constants.MailTemplates.MT2_ChangePassword, UserId, Constants.Systems.ZENIT7);

                    return new Result.OKResult(successfulNotification);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string error = message.ErrorMessage != string.Empty
                        ? message.ErrorMessage
                        : message.Exception?.Message;

                    if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                            .ToLower() == "true")
                        Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                        .ToLower() == "true")
                    Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404",
                    ex.InnerException != null ? ex.InnerException.ToString() : ex.Message.ToString());
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        [HttpGet]
        [Route("ProvidersNotifyCompleteFile")]
        public async Task<IActionResult> GetDataProvidersNotifyCompleteCheckIn(int UserId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var successfulNotification = false;

                    successfulNotification = await _notificationEmailService.GetDataProvidersNotifyCompleteCheckIn(Constants.MailTemplates.MT1_ConfirmRegistration, UserId, Constants.Systems.ZENIT7);

                    return new Result.OKResult(successfulNotification);
                }
                else
                {
                    ModelError message = ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e).First();

                    string error = message.ErrorMessage != string.Empty
                        ? message.ErrorMessage
                        : message.Exception?.Message;

                    if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                            .ToLower() == "true")
                        Log.Error(error, "ModelState is invalid.");

                    return new Result.BadRequestResult("400", error);
                }
            }
            catch (Exception ex)
            {
                if (Configuration.GetSection("ApplicationSettings").GetSection("DevelopmentLogTxt").Value.ToString()
                        .ToLower() == "true")
                    Log.Fatal(ex, "Host terminated unexpectedly.");

                return new Result.BadRequestResult("404",
                    ex.InnerException != null ? ex.InnerException.ToString() : ex.Message.ToString());
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

       
    }
}