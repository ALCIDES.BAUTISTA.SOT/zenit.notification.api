﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Zenit.Notification.API.Result
{
    public class BadRequestResult : IActionResult
    {
        private readonly Error _error;

        public BadRequestResult(string _code, string _message)
        {
            _error = new Error()
            {
                code = _code,
                message = _message
            };
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            BadRequest _errorContent = new BadRequest();
            _errorContent.error = _error;

            var objectResult = new ObjectResult(_errorContent)
            {
                StatusCode = StatusCodes.Status400BadRequest
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }

    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class BadRequest
    {
        public Error error { get; set; }
    }
}
